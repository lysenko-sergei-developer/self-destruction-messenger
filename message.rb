require 'dm-core'
require 'dm-migrations'
require 'base64'
require 'aes'
require 'data_mapper'

class Message
  include DataMapper::Resource
    
  property :id, Serial
  property :user_message, Object
  property :destroy_link, Integer
  property :destroy_hour, Integer
  property :create_time, DateTime
  property :route, String
  property :key_message, Object
end

helpers do
  include Base64
  
  def encrypt(message)
    key = AES.key
    encrypt_message = AES.encrypt(message, key)

    return encrypt_message.bytes,
           key.bytes
  end

  def decrypt(encrypt_message, key_message)
    decrypt_message = AES.decrypt(encrypt_message, key_message)
  
    return decrypt_message
  end
end

post '/' do
  @message = Message.create(params[:message])

  encrypt_message, key = encrypt(@message.user_message)

  @message.update(:user_message => encrypt_message,
                  :key_message => key)

  slim :link__message
end

get '/message/*' do
  @message = Message.first(:route => params['splat'])
  @show_once = @message
  
  if @message
    time_now = Time.now.to_i
    time_destruct = @message.create_time.to_time.to_i + 3600
    
    encrypt_message = @show_once.user_message
    key = @show_once.key_message

    encrypt_message = encrypt_message.map {|x| x.chr}.join
    key = key.map {|x| x.chr}.join

    @show_once.user_message = decrypt(encrypt_message, key) 

    if (@message.destroy_hour == 1) && (time_now > time_destruct)
      Message.get(@message.id).destroy
      redirect to not_found
    elsif @message.destroy_link == 1 && @message
      Message.get(@message.id).destroy
    end

    slim :user__message, :layout =>  false
  else 
    redirect to not_found
  end
end

DataMapper.finalize

