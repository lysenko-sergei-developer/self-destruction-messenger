require 'sinatra'
require 'sinatra/reloader' if development?
require 'slim'
require './message.rb'

configure :development do
  DataMapper.setup(:default, "sqlite3://#{Dir.pwd}/development.db")
  DataMapper.auto_migrate!
end

configure :production do
  DataMapper.setup(:default, ENV['HEROKU_POSTGRESQL_RED_URL'])
end

get '/' do
  @message = Message.new
  slim :home
end

not_found do 
  slim :not_found
end